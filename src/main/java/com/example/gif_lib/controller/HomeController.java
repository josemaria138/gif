package com.example.gif_lib.controller;



import com.example.gif_lib.dao.Search;
import com.example.gif_lib.model.Category;
import com.example.gif_lib.model.Gif;
import com.example.gif_lib.dao.GifDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class HomeController {

    @Autowired
    private GifDao gifDao;
    @Autowired
    private Search search;

    @GetMapping("/")
    public String show(ModelMap map){
      map.put("gifsUrl", gifDao.getUrl(Gif.getAllGifs()));

        return "home";
    }

    @RequestMapping("/searchGif")
    public String search(@RequestParam(required = false) String input, ModelMap modelMap){
        modelMap.put("gifsUrl", search.search(input));
        return "search";
    }

    @GetMapping("api/categories/{id}")
    @ResponseBody
    public Category getCategoryJson(@PathVariable int id){
        return Category.getAllCategories().get(id);
    }


    @GetMapping("api/gifs")
    @ResponseBody
    public List<Gif> getAllGifsJson() {
            return Gif.getAllGifs();

    }


}