package com.example.gif_lib.controller;


import com.example.gif_lib.model.Category;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class CategoryController {


    @GetMapping("/categories")
    public String myCategories(ModelMap modelMap){
        modelMap.put("categories", Category.getAllCategories());
        return "categories";
    }

    @GetMapping("/category/{id}")
    public String myCategory(@PathVariable int id, ModelMap modelMap ){
        Category category = Category.getAllCategories().get(id);
        modelMap.put("category", category);
        modelMap.put("gifsUrl",Category.getGifsByCategory(category));
        return "category";
    }

}
